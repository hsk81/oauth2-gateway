#!/usr/bin/env python
###############################################################################

from setuptools import setup

###############################################################################
###############################################################################

setup(
    name='oauth2-gateway',
    version='1.0.0',
    description='OAuth2 gateway',
    author='Hasan Karahan',
    author_email='hasan.karahan@blackhan.com',
    url='https://github.org/hsk81/oauth2-gateway',
    install_requires=[
        'falcon>=1.0.0',
        'gunicorn>=19.6.0',
        'pytest>=3.0.5',
        'six>=1.10.0',
        'requests>=2.12.3'
    ],
)

###############################################################################
###############################################################################
